import {createStore, combineReducers} from 'redux'
import uuid from 'uuid'
// import { strictEqual } from 'assert';

// ADD_EXPENSE
const addExpense = (
    {
        description = '',
        note='', 
        amount=0, 
        createdAt=0
    }={}) => (
    {
        type: 'ADD_EXPENSE',
        expense: {
            id: uuid(),
            description: description,
            note: note,
            amount: amount,
            createdAt: createdAt,

        }
    }
)

// REMOVE_EXPENSE
const removeExpense = ({id}) => (
    {
        type:'REMOVE_EXPENSE',
        id
    }
)

// EDIT_EXPENSE
const editExpense = (id, updates) => ({
    type: 'EDIT_EXPENSE',
    id,
    updates
})

// SET_TEXT_FILTER
const setTextFilter = (text='') => ({
    type: 'SET_TEXT_FILTER',
    text
})

// SORT_BY_DATE
const sortByDate = () => ({
    type: 'SORT_BY_DATE'
})

// SORT_BY_AMOUNT
const sortByAmount = () => ({
    type: 'SORT_BY_AMOUNT'
})

// SET_START_DATE
const setStartDate = (startDate = undefined) => ({
    type: 'SET_START_DATE',
    startDate
})

// SET_END_DATE
const setEndDate = (endDate = undefined) => ({
    type: 'SET_END_DATE',
    endDate
})

const expensesReducerDefaultState = [];
const expensesReducer = (state = expensesReducerDefaultState, action) => {
    switch (action.type) {
        case 'ADD_EXPENSE':
            return [
                ...state,
                action.expense
            ];
        case 'REMOVE_EXPENSE':
              return state.filter((expense)=>{
                  return expense.id !== action.id
              })
             
        case 'EDIT_EXPENSE':
            return state.map((expense)=>{
                if (expense.id === action.id) {
                   return { 
                       ...expense,
                       ...action.updates
                    }
                } else {
                    return expense
                }
            })
        default:
            return state
    }
}

const filtersReducerDefaultState = {
    text: '',
    sortBy: 'amount',
    startDate: undefined,
    endDate: undefined
}

const filtersReducer = (state = filtersReducerDefaultState, action) => {
    switch (action.type) {
        case 'SET_TEXT_FILTER':
            return {
                ...state,
                text: action.text,
            }
        case 'SORT_BY_DATE':
            return {
                ...state,
                sortBy: 'date'
            }
        
        case 'SORT_BY_AMOUNT':
            return {
                ...state,
                sortBy: 'amount'
            }
        
        case 'SET_START_DATE':
            return {
                ...state,
                startDate: action.startDate
            }
        
        case 'SET_END_DATE':
            return {
                ...state,
                endDate: action.endDate
            }
        default: 
        return state
    }
}

// GET visible expenses
const getVisibleExpenses = (expenses, {text, sortBy, startDate, endDate}) => {
    return expenses.filter((expense)=>{
        const startDateMatch = typeof endDate !== 'number' || expense.createdAt >= startDate
        const endDateMatch = typeof endDate !== 'number' || expense.createdAt <= endDate
        const textMatch =  expense.description.toLowerCase().includes(text.toLowerCase())

        return startDateMatch && endDateMatch && textMatch
    }).sort((a, b)=>{
        if (sortBy === 'date') {
            return a.createdAt < b.createdAt ? 1 : -1
        }

        if (sortBy === 'amount') {
            return a.amount < b.amount ? 1:-1
        }
    })
}

// Store Creation
const store = createStore(
    combineReducers({
        expenses: expensesReducer,
        filters: filtersReducer
    })
);

store.subscribe(()=>{
    const state = store.getState()
    const visibleExpenses = getVisibleExpenses( state.expenses, state.filters)
    console.log(visibleExpenses)
})




// // DISPATCHES
const expense1 = store.dispatch(addExpense({description: 'Rent', amount: 100, createdAt:5}))

const expense2 = store.dispatch(addExpense({description: 'Coffee', amount: 200, createdAt: 5}))
const expense3 = store.dispatch(addExpense({description: 'beauty products', amount: 300, createdAt: 3}))
const expense4 = store.dispatch(addExpense({description: 'Rent', amount: 400, createdAt:9}))

// store.dispatch(removeExpense({id:expense2.expense.id}))
// store.dispatch(editExpense(expense2.expense.id, {amount:550}))

// store.dispatch(setTextFilter('rent'));
// store.dispatch(setTextFilter());
// store.dispatch(setTextFilter(''))

// store.dispatch(sortByAmount())
// store.dispatch(sortByDate())

// store.dispatch(setStartDate(5))
// store.dispatch(setEndDate(0))
// store.dispatch(setEndDate(6))

const demoState = {
    expenses: [{
        id: 'id-number',
        description: 'January Rend',
        note: 'This was the final payment for that address',
        amount: 54500,
        createdAt: 0,
    }],
    filters: {
        text: 'rent`',
        sortBy: 'amount', //Date or Amount
        startDate: undefined,
        endDate: undefined
    }
}



// const user ={
//     name: 'Jen',
//     age: 25
// }

// console.log({
//     ...user,
//     location: 'Philadelphia',
//     age: 27
// })