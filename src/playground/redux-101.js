import {createStore} from 'redux'

// Action Generators  
const add =({ a, b }, c)=> {
    return a + b + c
}
console.log(add({a: 1, b: 12}, 100))

const incrementCount = ({incrementBy = 1} = {}) => ({
        type: 'INCREMENT',
        incrementBy
    })

const decrementCount = ({decrementBy = 1} = {}) => ({
        type: 'DECREMENT',
        decrementBy
})

const setCount = ({setCountBy = 0} = {}) => ({
    type: 'SET',
    setCountBy
})

const reset = () => ({
    type: 'RESET',
    count: 0
})

//Reducers
const countReducer = (state = { count:0 }, action) => {
    switch (action.type) {
        case 'INCREMENT':
            const incrementBy = typeof action.incrementBy === 'number' ? action.incrementBy : 1
            return {
            count: state.count + action.incrementBy
       }

       case 'DECREMENT':
            const decrementBy = typeof action.decrementBy === 'number' ? action.decrementBy : 1
           return {
               count: state.count - action.decrementBy
           }

        case 'RESET':
            return {
                count: 0
            }
        
        case 'SET':
            return {
                count: action.setCountBy
            }
        
       default: 
       return state
    }
}

const store = createStore(countReducer)

const unsubscsribe = store.subscribe(()=> [
    console.log(store.getState())
])

store.dispatch(incrementCount({incrementBy:5}))

store.dispatch({
    type: 'RESET'
})

// store.dispatch({
//     type: 'DECREMENT'
// });

store.dispatch(decrementCount({decrementBy:10}));


store.dispatch(setCount({setCountBy: -101}))

store.dispatch(reset())
