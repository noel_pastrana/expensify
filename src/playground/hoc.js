// Highter order component (HOC) - A component(HOC) that renders another component
// Reuse Code
// Render hijacking
// Prop Manipultion
// Abstract State

import React from 'react';
import ReactDOM from 'react-dom'

const Info = (props) => (
    <div>
        <h1>Info</h1>
        <p>the info is: {props.info}</p>
    </div>
)

const  withAdminWarning = (Wrappedcomponent) => {
    return (props) => (
        <div>
            {props.isAdmin && <p>this is a private info please done share!</p>}            
            <Wrappedcomponent {...props}/>
        </div>
    )
}

const  requireAuthentication = (Wrappedcomponent) => {
    return (props) => (
        <div>
            {props.isAuthenticated ? (
                <Wrappedcomponent {...props}/>
            ) : (
                <p>Please Login Here!</p>
            )}            
        </div>
    )
}

const AdminInfo = withAdminWarning(Info)
const AuthInfo = requireAuthentication(Info)

// ReactDOM.render(<AdminInfo isAdmin={true} info="These are the details" />, document.querySelector('#app'))
ReactDOM.render(<AuthInfo isAuthenticated={false} info="These are the details" />, document.querySelector('#app'))
