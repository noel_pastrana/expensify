import React from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import Header from '../components/Header'
import ExpenseDashboardpage from '../components/ExpenseDashboardPage'
import PageNotFound from '../components/PageNotFound'
import AddExpensePage from '../components/AddExpensePage'
import EditExpensePage from '../components/EditExpensePage'
import HelpPage from '../components/HelpPage'


const AppRouter = () => (
    <BrowserRouter>
        <div>
            <Header/>
        </div>

        <Switch>
            <Route path="/" component={ExpenseDashboardpage} exact={true} />
            <Route path="/create" component={AddExpensePage} />
            <Route path="/edit/:id" component={EditExpensePage} />
            <Route path="/help" component={HelpPage} />
            <Route component={PageNotFound}/>
        </Switch>
    </BrowserRouter>
)

export default AppRouter

