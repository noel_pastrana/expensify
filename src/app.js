import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import AppRouter, {AuthRouter} from './routers/AppRouter'
import configureStore from './store/configureStore'
import {addExpense, editExpense, removeExpense} from './actions/expenses'
import {setTextFilter} from './actions/filters'
import getVisibleExpenses from './selectors/expenses'
import 'normalize.css/normalize.css'
import './styles/styles.scss'

const store = configureStore()

const expense1 = store.dispatch(addExpense({description: 'Water Bill', amount: 100, createdAt:300}))
const expense2 = store.dispatch(addExpense({description: 'Gas Bill', amount: 200, createdAt: 200}))
const expense3 = store.dispatch(addExpense({description: 'Rent', amount: 300, createdAt: 100}))


const state = store.getState()
const visibleExpenses = getVisibleExpenses( state.expenses, state.filters)
console.log(visibleExpenses)

const jsx = (
        <Provider store={store}>
            <AppRouter />
        </Provider>
    )


ReactDOM.render(jsx, document.getElementById('app'))