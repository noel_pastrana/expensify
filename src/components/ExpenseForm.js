import React from 'react'
import moment from 'moment'
import "react-dates/initialize";
import { SingleDatePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";

export default class ExpenseForm extends React.Component {
    state = {
        description: '',
        note: '',
        amount: '',
        createdAt: moment(),
        focused: false,
        error: ''
    }

    onDescriptionChange = (e) => {
        const description = e.target.value
        this.setState(()=>({description}))
    }

    onNoteChange = (e) =>{
        const note = e.target.value
        this.setState(()=>({note}))  
    }

    onAmountChange = (e) => {
        const amount = e.target.value
        const regx2 = /^\d{1,}(\.\d{0,2})?$/
        if (!amount ||amount.match(regx2)) {
            this.setState(()=>({amount}))
        }
    }

    onDateChange = (createdAt) => {
        if(createdAt) {
            this.setState(()=>({createdAt}))
         }
    }

    onFocusChange = ({focused}) => {
        this.setState(()=>({focused: focused}))
    }

    onSubmit = (e) => {
        e.preventDefault()
        if (!this.state.description || !this.state.amount) {
            this.setState(()=>({error:'Please provide description and amount'}))
        } else {
            // Clear the error
            this.setState(()=>({error:''}))
            this.props.onSubmit({
                description:this.state.description,
                amount: parseFloat(this.state.amount, 10) * 100,
                createdAt: this.state.createdAt.valueOf()
            })
        }
    }

    render() {
        return (
            <div>
                {this.state.error && <p>{this.state.error}</p>}
                <form onSubmit={this.onSubmit}>
                    <input 
                        type="text" 
                        placeholder="Description" 
                        autoFocus 
                        value={this.state.description}  
                        onChange={this.onDescriptionChange}
                        
                        />
                    <input 
                        type="text" 
                        placeholder="amount" 
                        value={this.state.amount}
                        onChange={this.onAmountChange}
                        />
                    <SingleDatePicker
                        date={this.state.createdAt}
                        onDateChange={createdAt => this.setState({ createdAt })}
                        focused={this.state.focused} // PropTypes.bool
                        onFocusChange={({ focused }) => this.setState({ focused })}
                        numberOfMonths={1}
                        isOutsideRange={() => false}
                    />
                    <textarea 
                        placeholder="Add a note for your expense"
                        onChange={this.onNoteChange}
                        value={this.state.note}
                        ></textarea>
                    <button  type="submit"> Add Expense </button>
                </form>
            </div>
        )
    }
}