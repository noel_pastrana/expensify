import React from 'react'
import {connect} from 'react-redux'
import {setTextFilter, sortByAmount, sortByDate} from '../actions/filters'

const ExpenseListFilters = (props) => {
    return (
        <div>
            <input type="text" onChange={(e)=>{
                props.dispatch(setTextFilter(e.target.value))
            }} defaultValue={props.filters.text}/>

            <select onChange={(e)=>{
                var tarVal = e.target.value

                if(tarVal === "date") {
                    props.dispatch(sortByDate())
                } 
                
                if (tarVal === "amount") {
                    props.dispatch(sortByAmount())
                }

                console.log(tarVal)
            }}>
                <option value="date">-</option>
                <option value="date">Date</option>
                <option value="amount">Amount</option>
            </select>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        filters: state.filters
    }
}

export default connect(mapStateToProps)(ExpenseListFilters)