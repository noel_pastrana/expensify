import React from 'react';

const EditExpensePage = (props) => {
    return (
    <div>
        Editing the contents of {props.match.params.id}
    </div>
)}

export default EditExpensePage